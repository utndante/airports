package com.tp5.controllers;

import com.tp5.models.Route;
import com.tp5.services.RouteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(RouteController.class)

public class RouteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RouteService routeService;

    private Route route;
    private List<Route> routes;

    @Before
    public void setUp() {
        routes = new ArrayList<>();

        Route route1 = new Route();
        Route route2 = new Route();

        route1.setId((long) 1);
        route2.setId((long) 2);


        routes.add(route1);
        routes.add(route2);

        route = new Route();
        route.setId((long) 3);
    }

    @Test
    public void index() {

        long origin = 1;
        long destination = 2;
        when(routeService.findByOriginAirportIdAndDestinationAirportId(origin,destination)).thenReturn(routes);

        try {
            this.mockMvc.perform(get("/routes?origin=1&destination=2").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void index1() {

        long origin = 1;
        when(routeService.findByOriginAirportId(origin)).thenReturn(routes);

        try {
            this.mockMvc.perform(get("/routes?origin=1&destination=").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void index2() {

        when(routeService.getAll()).thenReturn(routes);

        try {
            this.mockMvc.perform(get("/routes?origin=&destination=").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void store() {
        when(routeService.save(route)).thenReturn(route);

        try {
            this.mockMvc.perform(
                    post("/routes")
                            .content("{\n" +
                                    "\t\"originAirport\" : {\n" +
                                    "\t\t\"id\" : \"1\"\n" +
                                    "\t},\n" +
                                    "\t\"destinationAirport\" : {\n" +
                                    "\t\t\"id\" : \"2\"\n" +
                                    "\t}\n" +
                                    "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void update() {
        when(routeService.update(route)).thenReturn(route);

        try{

            this.mockMvc.perform(
                    patch("/routes/1")
                            .content("{\n" +
                                    "\t\"originAirport\" : {\n" +
                                    "\t\t\"id\" : \"2\"\n" +
                                    "\t},\n" +
                                    "\t\"destinationAirport\" : {\n" +
                                    "\t\t\"id\" : \"3\"\n" +
                                    "\t}\n" +
                                    "}")
                            .accept("application/json")
                            .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isOk());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void remove() {
        doNothing().when(routeService).delete(route);

        try{
            this.mockMvc.perform(
                    delete("/routes/1").accept("application/json"))
                    .andExpect(status().isNoContent());
            //verify(cabinService.delete(cabin),times(1));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
