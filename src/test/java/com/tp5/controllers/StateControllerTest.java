package com.tp5.controllers;

import com.tp5.models.State;
import com.tp5.services.StateService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(StateController.class)

public class StateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StateService stateService;

    private State state;
    private List<State> states;

    @Before
    public void setUp() {
        states = new ArrayList<>();

        State state1 = new State();
        State state2 = new State();

        state1.setId((long) 1);
        state2.setId((long) 2);


        states.add(state1);
        states.add(state2);

        state = new State();
        state.setId((long) 3);
    }

    @Test
    public void index() {

        when(stateService.getAll()).thenReturn(states);

        try {
            this.mockMvc.perform(get("/countries/1/states").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {
        long id = 1;
        when(stateService.getById(id)).thenReturn(Optional.of(state));

        try {
            this.mockMvc.perform(get("/countries/1/states/1").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void store() {
        when(stateService.save(state)).thenReturn(state);

        try {
            this.mockMvc.perform(
                    post("/countries/1/states")
                            .content("{\n" +
                                    "\t\"iata\" : \"abc\",\n" +
                                    "\t\"name\" : \"rio\"\n" +
                                    "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
