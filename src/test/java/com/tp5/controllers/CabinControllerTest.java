package com.tp5.controllers;

import com.tp5.models.Cabin;
import com.tp5.services.CabinService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(CabinController.class)

public class CabinControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CabinService cabinService;

    private Cabin cabin;
    private List<Cabin> cabins;

    @Before
    public void setUp() {
        cabins = new ArrayList<>();

        Cabin cabin1 = new Cabin();
        Cabin cabin2 = new Cabin();

        cabin1.setId((long) 1);
        cabin2.setId((long) 2);


        cabins.add(cabin1);
        cabins.add(cabin2);

        cabin = new Cabin();
        cabin.setId((long) 3);
    }

    @Test
    public void index() {

        when(cabinService.getAll()).thenReturn(cabins);

        try {
            this.mockMvc.perform(get("/cabins").accept("application/json"))
                    .andExpect(status().isOk());
            verify(cabinService,times(1)).getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {
        long id = 1;
        when(cabinService.getById(id)).thenReturn(Optional.of(cabin));

        try {
            this.mockMvc.perform(get("/cabins/1").accept("application/json"))
                    .andExpect(status().isOk());
            verify(cabinService,times(1)).getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void store() {
        when(cabinService.save(cabin)).thenReturn(cabin);

        try {
            this.mockMvc.perform(
                    post("/cabins")
                            .content("{\n" +
                                "\t\"name\":\"cb\"\n" +
                            "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void update() {
        when(cabinService.update(cabin)).thenReturn(cabin);
        try{
            this.mockMvc.perform(
                    patch("/cabins/2")
                        .content("{\n" +
                            "\t\"name\":\"ab\"\n" +
                            "}")
                        .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void remove() {
        doNothing().when(cabinService).delete(cabin);

        try{
            this.mockMvc.perform(
                    delete("/cabins/3").accept("application/json"))
                    .andExpect(status().isNoContent());
            verify(cabinService,times(1)).delete(refEq(cabin));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
