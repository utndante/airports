package com.tp5.controllers;

import com.tp5.models.Flight;
import com.tp5.services.FlightService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(FlightController.class)

public class FlightControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightService flightService;

    private Flight flight;
    private List<Flight> flights;

    @Before
    public void setUp() {
        flights = new ArrayList<>();

        Flight route1 = new Flight();
        Flight route2 = new Flight();

        route1.setId((long) 1);
        route2.setId((long) 2);


        flights.add(route1);
        flights.add(route2);

        flight = new Flight();
        flight.setId((long) 3);
    }

    @Test
    public void index() {

        when(flightService.getAll()).thenReturn(flights);

        try {
            this.mockMvc.perform(get("/flights").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {
        long id = 1;
        when(flightService.getById(id)).thenReturn(Optional.of(flight));

        try {
            this.mockMvc.perform(get("/flights/1").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void store() {
        when(flightService.save(flight)).thenReturn(flight);

        try {
            this.mockMvc.perform(
                    post("/flights")
                            .content("{\n" +
                            "\t\"cabin\" : {\n" +
                            "\t\t\"id\" : \"4\",\n" +
                            "\t\t\"route\" : {\n" +
                            "\t\t\"id\" : \"5\"\n" +
                            "\t\t}\n" +
                            "\t}\n" +
                            "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void update() {
        when(flightService.update(flight)).thenReturn(flight);

        try{

            this.mockMvc.perform(
                    put("/flights/1")
                            .content("{\n" +
                                    "\t\"cabin\" : {\n" +
                                    "\t\t\"id\" : \"2\",\n" +
                                    "\t\t\"route\" : {\n" +
                                    "\t\t\"id\" : \"1\"\n" +
                                    "\t\t}\n" +
                                    "\t}\n" +
                                    "}")
                            .accept("application/json")
                            .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isOk());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void remove() {
        doNothing().when(flightService).delete(flight);

        try{
            this.mockMvc.perform(
                    delete("/flights/1").accept("application/json"))
                    .andExpect(status().isNoContent());
            //verify(flightService,times(1)).delete(refEq(flight));

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
