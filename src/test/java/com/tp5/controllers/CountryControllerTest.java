package com.tp5.controllers;

import com.tp5.models.Country;
import com.tp5.services.CountryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CountryController.class)
public class CountryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CountryService countryService;

    private Country country;
    private List<Country> countries;

    @Before
    public void setUp() {
        countries = new ArrayList<>();

        Country country1 = new Country();
        Country country2 = new Country();

        country1.setId((long) 1);
        country2.setId((long) 2);

        countries.add(country1);
        countries.add(country2);

        country = new Country();
        country.setId((long) 3);
    }


    @Test
    public void index(){
        List<Country> countries = new ArrayList<>();
        Country c1 = new Country();
        Country c2 = new Country();

        countries.add(c1);
        countries.add(c2);

        when(countryService.getAll()).thenReturn(countries);

        try {
        mockMvc.perform(get("/countries").accept("application/json"))
                .andExpect(status().isOk());
        verify(countryService,times(1)).getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void show(){
        when(countryService.getById(Long.parseLong("1"))).thenReturn(Optional.of(new Country()));

        try {
        mockMvc.perform(get("/countries/1").accept("application/json"))
                .andExpect(status().isOk());
        verify(countryService,times(1)).getById(Long.parseLong("1"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void store(){
        when(countryService.save(country)).thenReturn(country);

        try {
        ResultActions result = mockMvc.perform(
                post("/countries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                            "\t\"name\": \"C1\",\n" +
                            "\t\"iso2\": \"C1\"\n" +
                            "}")
        );
        result.andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
