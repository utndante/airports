package com.tp5.controllers;

import com.tp5.models.Price;
import com.tp5.services.PriceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PriceController.class)

public class PriceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PriceService priceService;

    private Price price;
    private List<Price> prices;

    @Before
    public void setUp() {
        prices = new ArrayList<>();

        Price price1 = new Price();
        Price price2 = new Price();

        price1.setId((long) 1);
        price2.setId((long) 2);


        prices.add(price1);
        prices.add(price2);

        price = new Price();
        price.setId((long) 3);
    }

    @Test
    public void index() {

        long flightId = 1;
        when(priceService.findByFlightId(flightId)).thenReturn(prices);

        try {
            this.mockMvc.perform(get("/flights/1/prices").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {

        long id = 1;
        when(priceService.getById(id)).thenReturn(Optional.of(price));

        try {
            this.mockMvc.perform(get("/flights/1/prices/1").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void store() {
        when(priceService.save(price)).thenReturn(price);

        try {
            this.mockMvc.perform(
                    post("/flights/1/prices")
                            .content("{\n" +
                                    "\t\"amount\" : \"100\",\n" +
                                    "\t\"from\" : \" 2018-06-03\",\n" +
                                    "\t\"to\" : \" 2018-06-17\"\n" +
                                    "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void update() {
        when(priceService.update(price)).thenReturn(price);

        try{

            this.mockMvc.perform(
                    put("/flights/1/prices/1")
                            .content("{\n" +
                                    "\t\"amount\" : \"50\",\n" +
                                    "\t\"from\" : \" 2000-04-03\",\n" +
                                    "\t\"to\" : \" 2018-06-17\"\n" +
                                    "}")
                            .accept("application/json")
                            .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isOk());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void remove() {

        doNothing().when(priceService).delete(price);

        try{
            this.mockMvc.perform(
                    delete("/flights/1/prices/2").accept("application/json"))
                    .andExpect(status().isNoContent());
            //verify(priceService,times(10)).delete(refEq(price));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
