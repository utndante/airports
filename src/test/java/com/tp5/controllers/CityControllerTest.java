package com.tp5.controllers;

import com.tp5.models.City;
import com.tp5.services.CityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(CityController.class)

public class CityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CityService cityService;

    private City city;
    private List<City> cities;

    @Before
    public void setUp() {
        cities = new ArrayList<>();

        City city1 = new City();
        City city2 = new City();

        city1.setId((long) 1);
        city2.setId((long) 2);


        cities.add(city1);
        cities.add(city2);

        city = new City();
        city.setId((long) 3);
    }

    @Test
    public void index() {

        when(cityService.getAll()).thenReturn(cities);

        try {
            this.mockMvc.perform(get("/countries/1/states/1/cities").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {
        long id = 1;
        when(cityService.getById(id)).thenReturn(Optional.of(city));

        try {
            this.mockMvc.perform(get("/countries/1/states/1/cities/1").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void store() {
        when(cityService.save(city)).thenReturn(city);

        try {
            this.mockMvc.perform(
                    post("/countries/1/states/1/cities")
                            .content("{\n" +
                                    "\t\"iata\" : \"ebc\",\n" +
                                    "\t\"name\" : \"mardel\"\n" +
                                    "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
