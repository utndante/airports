package com.tp5.controllers;

import com.tp5.models.Airport;
import com.tp5.models.City;
import com.tp5.models.Country;
import com.tp5.models.State;
import com.tp5.services.AirportService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(AirportController.class)

public class AirportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AirportService airportService;

    private Airport airport;
    private List<Airport> airports;

    @Before
    public void setUp() {
        airports = new ArrayList<>();

        Airport city1 = new Airport();
        Airport city2 = new Airport();

        city1.setId((long) 1);
        city2.setId((long) 2);


        airports.add(city1);
        airports.add(city2);

        airport = new Airport();
        airport.setId((long) 3);
    }

    @Test
    public void index() {

        when(airportService.getAll()).thenReturn(airports);

        try {
            this.mockMvc.perform(get("/countries/1/states/1/cities/1/airports").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void show() {
        long id = 1;
        when(airportService.getById(id)).thenReturn(Optional.of(airport));

        try {
            this.mockMvc.perform(get("/countries/1/states/1/cities/1/airports/1").accept("application/json"))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void store() {
        when(airportService.save(airport)).thenReturn(airport);

        try {
            this.mockMvc.perform(
                    post("/countries/1/states/1/cities/1/airports")
                            .content("{\n" +
                                    "\t\"iata\" : \"aebc\",\n" +
                                    "\t\"name\" : \"airmardel\"\n" +
                                    "}")
                            .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void update() {
        when(airportService.update(airport)).thenReturn(airport);

        try{

            this.mockMvc.perform(
                    patch("/countries/1/states/1/cities/1/airports/1")
                            .content("{\n" +
                                    "\t\"iata\" : \"hijk\",\n" +
                                    "\t\"name\" : \"airbahia\"\n" +
                                    "}")
                            .accept("application/json")
                            .contentType(MediaType.APPLICATION_JSON)
            )
                    .andExpect(status().isOk());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void remove() {

        doNothing().when(airportService).delete(airport);

        try{
            this.mockMvc.perform(
                    delete("/countries/1/states/1/cities/1/airports/1").accept("application/json"))
                    .andExpect(status().isNoContent());
            //verify(airportService,times(1)).delete(refEq(airport));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
