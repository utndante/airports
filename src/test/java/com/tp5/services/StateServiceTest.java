package com.tp5.services;

import com.tp5.models.State;
import com.tp5.repositories.StateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class StateServiceTest {

        @Mock
        private CrudRepository crudRepository;

        @Mock
        private StateRepository stateRepository;

        @InjectMocks
        private StateService stateService;

        @Before
        public void setup(){
            MockitoAnnotations.initMocks(this);
        }

        @Test
        public void testGetAll(){
            List<State> states = new ArrayList<State>();
            states.add(new State());
            states.add(new State());
            states.add(new State());
            when(crudRepository.findAll()).thenReturn(states);

            List<State> result = (List<State>) crudRepository.findAll();
            assertEquals(3, result.size());
        }

        @Test
        public void testGetCountryById(){

            long id1 = 1;
            long id2 = 2;
            long id3 = 3;
            List<State> states = new ArrayList<State>();
            State state1 = new State();
            State state2 = new State();
            State state3 = new State();
            state1.setId(id1);
            state2.setId(id2);
            state3.setId(id3);
            states.add(state1);
            states.add(state2);
            states.add(state3);

            when(stateService.getByCountryId(id1)).thenReturn(states);

            List<State> result = (List<State>) stateService.getByCountryId(id1);
            assertEquals(3, result.size());
        }

}

