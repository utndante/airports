package com.tp5.services;

import com.tp5.models.Country;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class CountryServiceTest {

    @Mock
    private CrudRepository crudRepository;

    @InjectMocks
    private CountryService countryService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<Country> countries = new ArrayList<Country>();
        countries.add(new Country());
        countries.add(new Country());
        countries.add(new Country());
        when(crudRepository.findAll()).thenReturn(countries);

        List<Country> result = (List<Country>) countryService.getAll();
        assertEquals(3, result.size());
    }

}
