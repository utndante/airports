package com.tp5.services;

import com.tp5.models.Country;
import com.tp5.models.Flight;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)

public class FlightServiceTest {


    @Mock
    private CrudRepository crudRepository;

    @InjectMocks
    private FlightService flightService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<Flight> flights = new ArrayList<Flight>();
        flights.add(new Flight());
        flights.add(new Flight());
        flights.add(new Flight());
        when(crudRepository.findAll()).thenReturn(flights);

        List<Flight> result = (List<Flight>) flightService.getAll();
        assertEquals(3, result.size());
    }

}
