package com.tp5.services;

import com.tp5.models.Cabin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)

public class CabinServiceTest {

        @Mock
        private CrudRepository crudRepository;

        @InjectMocks
        private CabinService cabinService;

        @Before
        public void setup(){
            MockitoAnnotations.initMocks(this);
        }

        @Test
        public void testGetAll(){
            List<Cabin> cabins = new ArrayList<Cabin>();
            cabins.add(new Cabin());
            cabins.add(new Cabin());
            cabins.add(new Cabin());
            when(crudRepository.findAll()).thenReturn(cabins);

            List<Cabin> result = (List<Cabin>) cabinService.getAll();
            assertEquals(3, result.size());
        }
}
