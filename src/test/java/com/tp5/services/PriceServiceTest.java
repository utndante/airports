package com.tp5.services;

import com.tp5.models.Price;
import com.tp5.models.State;
import com.tp5.repositories.PriceRepository;
import com.tp5.repositories.StateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class PriceServiceTest {

    @Mock
    private CrudRepository crudRepository;

    @Mock
    private PriceRepository priceRepository;

    @InjectMocks
    private PriceService priceService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<Price> prices = new ArrayList<Price>();
        prices.add(new Price());
        prices.add(new Price());
        prices.add(new Price());
        when(crudRepository.findAll()).thenReturn(prices);

        List<Price> result = (List<Price>) crudRepository.findAll();
        assertEquals(3, result.size());
    }

    @Test
    public void testGetByFlight(){

        long id1 = 1;
        long id2 = 2;
        long id3 = 3;
        List<Price> prices = new ArrayList<Price>();
        Price price1 = new Price();
        Price price2 = new Price();
        Price price3 = new Price();
        price1.setId(id1);
        price2.setId(id2);
        price3.setId(id3);
        prices.add(price1);
        prices.add(price2);
        prices.add(price3);

        when(priceService.findByFlightId(id1)).thenReturn(prices);

        List<Price> result = (List<Price>) priceService.findByFlightId(id1);
        assertEquals(3, result.size());
    }

}
