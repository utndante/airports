package com.tp5.services;

import com.tp5.models.Route;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class RouteServiceTest {

    @Mock
    private CrudRepository crudRepository;

    @InjectMocks
    private RouteService routeService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<Route> routes = new ArrayList<Route>();
        routes.add(new Route());
        routes.add(new Route());
        routes.add(new Route());
        when(crudRepository.findAll()).thenReturn(routes);

        List<Route> result = (List<Route>) routeService.getAll();
        assertEquals(3, result.size());
    }

    @Test
    public void testGetByAirportId(){

        long id1 = 1;
        long id2 = 2;
        List<Route> routes = new ArrayList<Route>();
        Route route1 = new Route();
        Route route2 = new Route();
        route1.setId(id1);
        route2.setId(id2);
        routes.add(route1);
        routes.add(route2);

        try{
            when(routeService.findByOriginAirportId(id1)).thenReturn(routes);
            List<Route> result = (List<Route>) routeService.findByOriginAirportId(id1);
            assertEquals(2, result.size());
        }
        catch (Exception e){

        }
    }

    @Test
    public void testGetByOriginAndDestination(){

        long id1 = 1;
        long id2 = 2;
        List<Route> routes = new ArrayList<Route>();
        Route route1 = new Route();
        Route route2 = new Route();
        route1.setId(id1);
        route2.setId(id2);
        routes.add(route1);
        routes.add(route2);

        try{
            when(routeService.findByOriginAirportIdAndDestinationAirportId(id1,id2)).thenReturn(routes);
            List<Route> result = (List<Route>) routeService.findByOriginAirportIdAndDestinationAirportId(id1,id2);
            assertEquals(2, result.size());
        }
        catch (Exception e){

        }
    }
}
