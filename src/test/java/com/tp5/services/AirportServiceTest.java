package com.tp5.services;

import com.tp5.models.Airport;
import com.tp5.models.Price;
import com.tp5.repositories.AirportRepository;
import com.tp5.repositories.PriceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)

public class AirportServiceTest {

    @Mock
    private CrudRepository crudRepository;

    @Mock
    private AirportRepository airportRepository;

    @InjectMocks
    private AirportService airportService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<Airport> airports = new ArrayList<Airport>();
        airports.add(new Airport());
        airports.add(new Airport());
        airports.add(new Airport());
        when(crudRepository.findAll()).thenReturn(airports);

        List<Price> result = (List<Price>) crudRepository.findAll();
        assertEquals(3, result.size());
    }

    @Test
    public void testGetByFlight(){

        long id1 = 1;
        long id2 = 2;
        long id3 = 3;
        List<Airport> airports = new ArrayList<Airport>();
        Airport airport1 = new Airport();
        Airport airport2 = new Airport();
        Airport airport3 = new Airport();
        airport1.setId(id1);
        airport2.setId(id2);
        airport3.setId(id3);
        airports.add(airport1);
        airports.add(airport2);
        airports.add(airport3);

        when(airportService.findByCityId(id1)).thenReturn(airports);

        List<Airport> result = (List<Airport>) airportService.findByCityId(id1);
        assertEquals(3, result.size());
    }
}
