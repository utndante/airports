package com.tp5.services;

import com.tp5.models.City;
import com.tp5.repositories.CityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class CityServiceTest {

    @Mock
    private CrudRepository crudRepository;

    @Mock
    private CityRepository cityRepository;

    @InjectMocks
    private CityService cityService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll(){
        List<City> states = new ArrayList<City>();
        states.add(new City());
        states.add(new City());
        states.add(new City());
        when(crudRepository.findAll()).thenReturn(states);

        List<City> result = (List<City>) crudRepository.findAll();
        assertEquals(3, result.size());
    }

    @Test
    public void testGetCountryById(){

        long id1 = 1;
        long id2 = 2;
        long id3 = 3;
        List<City> cities = new ArrayList<City>();
        City city1 = new City();
        City city2 = new City();
        City city3 = new City();
        city1.setId(id1);
        city2.setId(id2);
        city3.setId(id3);
        cities.add(city1);
        cities.add(city2);
        cities.add(city3);

        when(cityService.findByStateId(id1)).thenReturn(cities);

        List<City> result = (List<City>) cityService.findByStateId(id1);
        assertEquals(3, result.size());
    }

}
