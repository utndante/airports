package com.tp5.controllers;

import com.tp5.models.Price;
import com.tp5.models.Flight;
import com.tp5.services.PriceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class PriceController {

    @Autowired
    private PriceService service;

    @GetMapping(path = "/flights/{id}/prices")
    public ResponseEntity index(Flight flight) {
        Iterable<Price> flights = service.findByFlightId(flight.getId());

        return ResponseEntity.status(HttpStatus.OK).body(flights);
    }

    @GetMapping(path = "/flights/{id}/prices/{id}")
    public ResponseEntity show(@PathVariable("id") long id) {
        Optional<Price> flight = service.getById(id);

        return ResponseEntity.status(HttpStatus.OK).body(flight);
    }

    @PostMapping(path = "/flights/{id}/prices")
    public ResponseEntity store(Flight flight, @RequestBody Price price) {
        price.setFlight(flight);
        Price response = service.save(price);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping(value = "/flights/{id}/prices/{idPrice}")
    public ResponseEntity update(@PathVariable("idPrice") Long idPrice, @RequestBody Price price) {
        price.setId(idPrice);
        service.update(price);

        return ResponseEntity.status(HttpStatus.OK).body(price);
    }

    @DeleteMapping(value = "flights/{id}/prices/{id}")
    public ResponseEntity delete(Flight flight, Price price) {
        service.delete(price);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

}
