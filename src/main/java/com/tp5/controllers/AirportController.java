package com.tp5.controllers;

import com.tp5.models.City;
import com.tp5.models.State;
import com.tp5.models.Country;
import com.tp5.models.Airport;
import com.tp5.services.AirportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class AirportController {

    @Autowired
    private AirportService service;

    @GetMapping(value = "/countries/{id}/states/{id}/cities/{id}/airports")
    public @ResponseBody ResponseEntity index(Country country, State state, City city) {
        Iterable<Airport> airports = service.findByCityId(city.getId());
        return ResponseEntity.status(HttpStatus.OK).body(airports);
    }

    @GetMapping(value = "/countries/{id}/states/{id}/cities/{id}/airports/{idAirport}")
    public ResponseEntity show(Country country, State state, City city, @PathVariable("idAirport") Long idAirport) {
        Optional<Airport> airport = service.getById(idAirport);
        return ResponseEntity.status(HttpStatus.OK).body(airport);
    }

    @PostMapping(value = "/countries/{id}/states/{id}/cities/{id}/airports")
    public ResponseEntity store(Country country, State state, City city, @RequestBody Airport airport) {
        airport.setCity(city);

        Airport response = service.save(airport);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(value = "/countries/{id}/states/{id}/cities/{id}/airports/{idAirport}")
    public ResponseEntity update(@PathVariable("idAirport") Long idAirport, @RequestBody Airport airport) {
        airport.setId(idAirport);

        service.update(airport);

        return ResponseEntity.status(HttpStatus.OK).body(airport);
    }

    @DeleteMapping(value = "/countries/{id}/states/{id}/cities/{id}/airports/{id}")
    public ResponseEntity delete(Country country, State state, City city, Airport airport) {
        service.delete(airport);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

}
