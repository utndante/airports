package com.tp5.controllers;

import com.tp5.models.City;
import com.tp5.models.State;
import com.tp5.models.Country;
import com.tp5.services.CityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class CityController {

    @Autowired
    private CityService service;

    @GetMapping(value = "/countries/{id}/states/{id}/cities")
    public @ResponseBody ResponseEntity index(Country country, State state) {
        Iterable<City> cities = service.findByStateId(state.getId());
        return ResponseEntity.status(HttpStatus.OK).body(cities);
    }

    @GetMapping(value = "/countries/{id}/states/{id}/cities/{idCity}")
    public ResponseEntity show(Country country, State state, @PathVariable("idCity") Long idCity) {
        Optional<City> city = service.getById(idCity);
        return ResponseEntity.status(HttpStatus.OK).body(city);
    }

    @PostMapping(value = "/countries/{id}/states/{id}/cities")
    public ResponseEntity store(Country country, State state, @RequestBody City city) {
        city.setState(state);

        City response = service.save(city);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

}
