package com.tp5.controllers;

import com.tp5.models.State;
import com.tp5.models.Country;
import com.tp5.services.StateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class StateController {

    @Autowired
    private StateService service;

    @GetMapping(value = "/countries/{id}/states")
    public @ResponseBody ResponseEntity index(Country country) {
        Iterable<State> states = service.getByCountryId(country.getId());
        return ResponseEntity.status(HttpStatus.OK).body(states);
    }

    @GetMapping(value = "/countries/{id}/states/{idState}")
    public ResponseEntity show(Country country, @PathVariable("idState") Long idState) {
        Optional<State> state = service.getById(idState);
        return ResponseEntity.status(HttpStatus.OK).body(state);
    }

    @PostMapping(value = "/countries/{id}/states")
    public ResponseEntity store(Country country, @RequestBody State state) {
        state.setCountry(country);

        State response = service.save(state);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

}
