package com.tp5.controllers;

import com.tp5.models.Route;
import com.tp5.services.RouteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class RouteController {
    
    @Autowired
    private RouteService service;
    
    @GetMapping(value = "/routes")
    public ResponseEntity index(@RequestParam(value = "origin", required = false) Long origin, @RequestParam(value = "destination", required = false) Long destination) {
        if (origin != null && destination != null) {
            Iterable<Route> routes = service.findByOriginAirportIdAndDestinationAirportId(origin, destination);
            return ResponseEntity.status(HttpStatus.OK).body(routes);
        } else if (origin != null) {
            Iterable<Route> routes = service.findByOriginAirportId(origin);
            return ResponseEntity.status(HttpStatus.OK).body(routes);
        } else {
            Iterable<Route> routes = service.getAll();
            return ResponseEntity.status(HttpStatus.OK).body(routes);
        }
    }
    
    @PostMapping(path = "/routes")
    public ResponseEntity store(@RequestBody Route route) {
        Route response = service.save(route);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
    
    @PatchMapping(value = "/routes/{id}")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody Route route) {
        route.setId(id);

        service.update(route);
        return ResponseEntity.status(HttpStatus.OK).body(route);
    }
    
    @DeleteMapping(value = "routes/{id}")
    public ResponseEntity delete(Route route) {
        service.delete(route);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }
}