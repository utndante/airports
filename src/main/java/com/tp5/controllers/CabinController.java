package com.tp5.controllers;

import com.tp5.models.Cabin;
import com.tp5.services.CabinService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class CabinController {

    @Autowired
    private CabinService service;

    @GetMapping(path = "/cabins")
    public @ResponseBody ResponseEntity index() {
        Iterable<Cabin> cabins = service.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(cabins);
    }

    @GetMapping(value = "/cabins/{id}")
    public ResponseEntity show(@PathVariable("id") long id) {
        Optional<Cabin> cabin = service.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(cabin);
    }

    @PostMapping(path = "/cabins")
    public ResponseEntity store(@RequestBody Cabin cabin) {
        Cabin response = service.save(cabin);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(value = "/cabins/{id}")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody Cabin cabin) {
        cabin.setId(id);
        service.update(cabin);
        return ResponseEntity.status(HttpStatus.OK).body(cabin);
    }

    @DeleteMapping(value = "cabins/{id}")
    public ResponseEntity delete(Cabin cabin) {
        service.delete(cabin);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

}