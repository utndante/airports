package com.tp5.controllers;

import com.tp5.models.Country;
import com.tp5.services.CountryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class CountryController {

    @Autowired
    private CountryService service;

    @GetMapping(path = "/countries")
    public @ResponseBody ResponseEntity index() {
        Iterable<Country> countries = service.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(countries);
    }

    @GetMapping(value = "/countries/{id}")
    public ResponseEntity show(@PathVariable("id") long id) {
        Optional<Country> country = service.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(country);
    }

    @PostMapping(path = "/countries")
    public ResponseEntity store(@RequestBody Country country) {
        Country response = service.save(country);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

}
