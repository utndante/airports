package com.tp5.controllers;

import com.tp5.models.Flight;
import com.tp5.services.FlightService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@RestController
public class FlightController {

    @Autowired
    private FlightService service;

    @GetMapping(path = "/flights")
    public ResponseEntity index() {
        Iterable<Flight> flights = service.getAll();

        return ResponseEntity.status(HttpStatus.OK).body(flights);
    }

    @GetMapping(value = "/flights/{id}")
    public ResponseEntity show(@PathVariable("id") long id) {
        Optional<Flight> flight = service.getById(id);

        return ResponseEntity.status(HttpStatus.OK).body(flight);
    }

    @PostMapping(path = "/flights")
    public ResponseEntity store(@RequestBody Flight flight) {
        Flight response = service.save(flight);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping(value = "/flights/{id}")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody Flight flight) {
        flight.setId(id);
        service.update(flight);

        return ResponseEntity.status(HttpStatus.OK).body(flight);
    }

    @DeleteMapping(value = "flights/{id}")
    public ResponseEntity delete(Flight flight) {
        service.delete(flight);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

}
