package com.tp5.repositories;

import com.tp5.models.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {
    Iterable<City> findByStateId(Long id);
}
