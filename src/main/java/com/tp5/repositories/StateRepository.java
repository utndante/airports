package com.tp5.repositories;

import com.tp5.models.State;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface StateRepository extends CrudRepository<State, Long> {
    Iterable<State> findByCountryId(Long id);
}
