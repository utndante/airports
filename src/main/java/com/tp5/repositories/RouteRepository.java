package com.tp5.repositories;

import com.tp5.models.Route;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface RouteRepository extends CrudRepository<Route, Long> {
    Iterable<Route> findByOriginAirportId(Long originId);
    Iterable<Route> findByOriginAirportIdAndDestinationAirportId(Long originId, Long destinationId);
}
