package com.tp5.repositories;

import com.tp5.models.Price;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface PriceRepository extends CrudRepository<Price, Long> {
    Iterable<Price> findByFlightId(Long id);
}
