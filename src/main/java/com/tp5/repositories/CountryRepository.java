package com.tp5.repositories;

import com.tp5.models.Country;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {
    //
}