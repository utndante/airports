package com.tp5.repositories;

import com.tp5.models.Airport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Long> {
    Iterable<Airport> findByCityId(Long id);
}