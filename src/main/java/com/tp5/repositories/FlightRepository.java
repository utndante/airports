package com.tp5.repositories;

import com.tp5.models.Flight;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface FlightRepository extends CrudRepository<Flight, Long> {
    //
}
