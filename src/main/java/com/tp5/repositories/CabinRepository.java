package com.tp5.repositories;

import com.tp5.models.Cabin;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface CabinRepository extends CrudRepository<Cabin, Long> {
    //
}
