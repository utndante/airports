package com.tp5.models;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "airports")
public class Airport {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false, name = "name", length = 50)
    private String name;

    @Column(nullable = false, name = "iata_code", length = 80)
    private String iata;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    /*@OneToMany(mappedBy = "originAirport", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("originAirport")
    private List<Route> originRoutes;

    @OneToMany(mappedBy = "destinationAirport", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("destinationAirport")
    private List<Route> destinationRoutes;*/

}
