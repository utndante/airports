package com.tp5.models;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "routes")
public class Route {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "airport_origin")
    private Airport originAirport;

    @ManyToOne
    @JoinColumn(name = "airport_destination")
    private Airport destinationAirport;

    @OneToMany(mappedBy = "route", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("route")
    private List<Flight> flights;

}
