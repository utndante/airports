package com.tp5.models;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "prices")
public class Price {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "amount", length = 100)
    private float amount;

    @Column(nullable = false, name = "from_date")
    private LocalDate from;

    @Column(nullable = false, name = "to_date")
    private LocalDate to;

    @ManyToOne
    @JoinColumn(name = "flight_id")
    private Flight flight;

}
