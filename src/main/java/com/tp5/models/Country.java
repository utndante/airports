package com.tp5.models;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "countries")
public class Country {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false, name = "name", length = 50)
    private String name;

    @Column(nullable = false, name = "iso2", length = 4)
    private String iso2;

    @OneToMany(mappedBy = "country", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("country")
    private List<State> states;

}
