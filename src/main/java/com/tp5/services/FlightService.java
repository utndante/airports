package com.tp5.services;

import com.tp5.models.Flight;
import com.tp5.repositories.FlightRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class FlightService extends BaseService<Flight> {

    @Autowired
    public FlightService(FlightRepository repository) {
        super(repository);
    }

}
