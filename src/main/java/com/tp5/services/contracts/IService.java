package com.tp5.services.contracts;

import java.util.Optional;

public interface IService<T> {

    Iterable<T> getAll();
    Optional<T> getById(Long id);
    T save(T data);
    T update(T data);
    void delete(T data);

}
