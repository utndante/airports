package com.tp5.services;

import com.tp5.services.contracts.IService;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public class BaseService<T> implements IService<T> {

    protected CrudRepository<T, Long> repository;

    public BaseService(CrudRepository<T, Long> repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<T> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<T> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public T save(T data) {
        return repository.save(data);
    }

    @Override
    public T update(T data) {
        return repository.save(data);
    }

    @Override
    public void delete(T data) {
        repository.delete(data);
    }

}
