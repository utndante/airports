package com.tp5.services;

import com.tp5.models.Cabin;
import com.tp5.repositories.CabinRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CabinService extends BaseService<Cabin> {

    @Autowired
    public CabinService(CabinRepository repository) {
        super(repository);
    }

}
