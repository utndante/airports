package com.tp5.services;

import com.tp5.models.Airport;
import com.tp5.services.contracts.IService;
import com.tp5.repositories.AirportRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Service
public class AirportService extends BaseService<Airport> {

    private AirportRepository repository;

    @Autowired
    public AirportService(AirportRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Iterable<Airport> findByCityId(Long id) {
        return repository.findByCityId(id);
    }

}

