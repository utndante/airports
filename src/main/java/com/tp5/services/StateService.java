package com.tp5.services;

import com.tp5.models.State;
import com.tp5.repositories.StateRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class StateService extends BaseService<State> {

    private StateRepository repository;

    @Autowired
    public StateService(StateRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Iterable<State> getByCountryId(Long id) {
        return repository.findByCountryId(id);
    }

}
