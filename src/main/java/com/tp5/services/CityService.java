package com.tp5.services;

import com.tp5.models.City;
import com.tp5.repositories.CityRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CityService extends BaseService<City> {

    private CityRepository repository;

    @Autowired
    public CityService(CityRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Iterable<City> findByStateId(Long id) {
        return repository.findByStateId(id);
    }

}
