package com.tp5.services;

import com.tp5.models.Country;
import com.tp5.repositories.CountryRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CountryService extends BaseService<Country> {

    @Autowired
    public CountryService(CountryRepository repository) {
        super(repository);
    }

}
