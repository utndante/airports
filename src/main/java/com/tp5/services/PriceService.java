package com.tp5.services;

import com.tp5.models.Price;
import com.tp5.repositories.PriceRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PriceService extends BaseService<Price> {

    private PriceRepository repository;

    @Autowired
    public PriceService(PriceRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Iterable<Price> findByFlightId(Long id) {
        return repository.findByFlightId(id);
    }
}
