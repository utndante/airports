package com.tp5.services;

import com.tp5.models.Route;
import com.tp5.repositories.RouteRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class RouteService extends BaseService<Route> {
    
    private RouteRepository repository;

    @Autowired
    public RouteService(RouteRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public Iterable<Route> findByOriginAirportId(Long originId) {
        return repository.findByOriginAirportId(originId);
    }

    public Iterable<Route> findByOriginAirportIdAndDestinationAirportId(Long originId, Long destinationId) {
        return repository.findByOriginAirportIdAndDestinationAirportId(originId, destinationId);
    }

}
