# Trabajo final Laboratorio V 2018
### API para sistema de aerolineas

### Consigna:
Para una aerolínea muy conocida, se nos requiere realizar una API REST para el mantenimiento de rutas de vuelos y los precios correspondientes.

### Antes de empezar
Dirigirse a la ruta `/src/main/resources` y copiar el archivo `application.properties.example` y pegarlo con el nombre `application.properties`.
Dentro de este archivo debe configurar sus datos de conexion a la base de datos.

### DER del sistema
![image der](https://i.imgur.com/tTqTtxT.png)

### Iniciar el sistema
```shell
mvn install - Instalar dependencias en el repositorio local.
mvn test -  Compilar y ejecutar los unit test.
mvn spring-boot: run - Inicia el servidor web APACHE TOMCAT con la API corriendo en el puerto 8080.
```
### Documentación de la API
Ingresar a la url `http://localhost:8080/swagger-ui.html`